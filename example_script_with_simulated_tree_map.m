close all
clear all

%In this example script, we simulate two sets of tree locations that differ
%from each other via an Euclidean transformation and small added random noise.
%After simulating the two sets of tree locations, we estimate the
%Euclidean transformation between the two datasets by using the coarse
%registration algorithm implemented in fit_euclidean_transformation.m.  

%Generate a simulated set of tree locations by sampling 2D coordinates 
%from a 2D uniform distribution .
x_center = 24; 
y_center = 103; 
width = 100; % m, width of the rectangular forest
stem_density = 500; %trees per ha
n_of_trees = round(stem_density * width^2 / (10^4));
xy_mat1 = width * (rand(n_of_trees, 2) - [0.5, 0.5]) + [x_center, y_center];

% Choose a subset of the trees. Here, we use a circle with a specific radius
r_set_2 = 20; %m
indices_within_circle = (xy_mat1(:,1) - x_center).^2 + (xy_mat1(:,2) - y_center).^2  < r_set_2^2;
xy_mat2 = xy_mat1(indices_within_circle, :); 
%Rotate the coordinates by a given angle and translate by a given vector
theta = 120/180 * pi; 
R_mat = [cos(theta), -sin(theta); sin(theta), cos(theta)];
t_vect = [20; -30]; 
xy_mat2_transf = (R_mat * xy_mat2' + t_vect)'; 

%Add some normally distributed noise to the locations
std_r = 0.25; %m standard deviation of noise in the radial direction
std_xy = std_r / sqrt(2);  %m standard deviation of noise in x and y direction
xy_mat2_transf_with_noise = xy_mat2_transf + std_xy * randn(size(xy_mat2_transf)); 

%Plot the tree locations before the registration 
figure; 
hold on
scatter(xy_mat1(:, 1), xy_mat1(:, 2), 35, 'or', 'filled')
scatter(xy_mat2_transf_with_noise(:, 1), xy_mat2_transf_with_noise(:, 2), 18, 'ok', 'filled')
set(gca, 'ticklength', 2*get(gca, 'ticklength'), 'Linewidth', 1.3, 'Fontsize', 16)
set(gca,'TickLabelInterpreter', 'latex')
xlabel('x (m)', 'Interpreter', 'latex', 'Fontsize', 18)
ylabel('y (m)', 'Interpreter', 'latex', 'Fontsize', 18)
leg = legend( 'Original trees', 'Transformed trees with noise', 'location', 'northwest');
set(leg, 'Interpreter', 'latex', 'Fontsize', 13)
title('Before registration','Interpreter', 'latex', 'Fontsize', 17);
box on
axis equal
ymin = min([min(xy_mat1(:, 2)), min(xy_mat2_transf_with_noise(:, 2))]) - 2; 
ymax = max([max(xy_mat1(:, 2)), max(xy_mat2_transf_with_noise(:, 2))]) + 15;
ylim([ymin, ymax])
pbaspect([1, 1, 1])


% Use the coarse registration algorithm to find the transformation
%  xy_2 = R_mat*xy_1 + t_vect. Note that xy_mat2_transf is used as the 2nd
%  dataset since it contains significantly less trees. 
parameters.R_local = 15;
parameters.k = 15; 
parameters.r_thres = 1.0; 
[theta_est, R_mat_est, t_vect_est, nof_matches, feat_desc_cell_array] = ...
    fit_euclidean_transformation(xy_mat1, xy_mat2_transf_with_noise, parameters); 
sprintf('[expected theta, estimated theta] = [%.5f, %.5f]', [theta, theta_est])
sprintf('[expected t_vect(1), estimated t_vect(1)] = [%.5f, %.5f]', [t_vect(1), t_vect_est(1)])
sprintf('[expected t_vect(2), estimated t_vect(2)] = [%.5f, %.5f]', [t_vect(2), t_vect_est(2)])
%Transforming the tree locations in the dataset 2 back to the original
%coordinate frame by using the estimated transformation parameters
xy_mat2_est = (R_mat_est' * (xy_mat2_transf_with_noise' - t_vect_est))'; 

%Plot the tree locations after the registration 
figure; 
hold on
scatter(xy_mat1(:, 1), xy_mat1(:, 2), 35, 'or', 'filled')
scatter(xy_mat2_est(:, 1), xy_mat2_est(:, 2), 18, 'ok', 'filled')
set(gca, 'ticklength', 2*get(gca, 'ticklength'), 'Linewidth', 1.3, 'Fontsize', 16)
set(gca,'TickLabelInterpreter', 'latex')
xlabel('x (m)', 'Interpreter', 'latex', 'Fontsize', 18)
ylabel('y (m)', 'Interpreter', 'latex', 'Fontsize', 18)
leg = legend( 'Original trees', 'Estimated tree locations', 'location', 'northwest');
set(leg, 'Interpreter', 'latex', 'Fontsize', 13)
title('After registration','Interpreter', 'latex', 'Fontsize', 17);
box on
axis equal
ymin = min([min(xy_mat1(:, 2)), min(xy_mat2_est(:, 2))]) - 2; 
ymax = max([max(xy_mat1(:, 2)), max(xy_mat2_est(:, 2))]) + 15;
ylim([ymin, ymax])
pbaspect([1, 1, 1])