# Efficient coarse registration method using translation- and rotation-invariant local descriptors  <br/>

## Introduction

This repository provides Matlab functions for implementing the 2D coarse registration algorithm proposed in Hyyppä and Muhojoki et
al (2021), "Efficient coarse registration method using translation- and rotation-invariant local descriptors towards fully automated 
forest inventory", ISPRS Open Journal of Photogrammetry and Remote Sensing. The algorithm has been designed to find the 2D Euclidean 
transformation between two point clouds in different coordinate systems by using the locations of some distinct objects detected 
from the point clouds. For example, we have used the registration algorithm to georeference a point cloud collected with a handheld 
laser scanner by finding the Euclidean transformation between the point cloud collected with the handheld scanner and a point cloud 
collected from the corresponding area with an airborne laser scanner. For this application, we have used trees as the relevant objects
meaning that the locations of the detected trees have been given as inputs to the registration algorithm. 

Dated: 25.10.2021
<div align="center">
    <img src="/Figures/graphical_abstract_coarse_registration.png" width="90%"/>
</div>

Please cite Hyyppä and Muhojoki et al (2021), "Efficient coarse registration method using translation- and rotation-invariant 
local descriptors towards fully automated forest inventory", if you use the Matlab code in this repository for your research,
teaching, business or for whatever other cause. 

## Brief description of the algorithm
The registration algorithm takes as input the locations of some distinct objects detected from the two point clouds that are 
to be registered. In the context of forestry, we have typically used the locations of trees for this purpose. 
The algorithm constructs a rotation- and translation-invariant feature descriptor vector for each of the detected objects 
based on the relative locations of the neighboring objects. Subsequently, the feature descriptors obtained for the 
different point clouds are compared against one another by using the Euclidean distance in the feature space as the similarity criterion. 
By using the nearest neighbor distance ratio, the most promising matching object pairs are found and further used to fit the optimal 
Euclidean transformation between the two point clouds. Importantly, the time complexity of the proposed algorithm scales quadratically 
in the number of objects detected from the point clouds making the algorithm truly efficient. For more details, see Hyyppä and Muhojoki et al (2021), 
"Efficient coarse registration method using translation- and rotation-invariant local descriptors towards fully automated forest inventory",
ISPRS Open Journal of Photogrammetry and Remote Sensing.

## Brief description of the Matlab functions and example scripts <br/>
The repository contains the following Matlab functions: <br/>
-fit_euclidean_transformation: implements the 2D coarse registration algorithm. Most important input variables for the function are the locations 
of objects, e.g. trees, detected from the two point clouds. <br/>
-find_matching_pairs: simple helper function that can be used to find matching object pairs after aligning the two point clouds. <br/>
The repository contains the following example scripts: <br/>
-example_script_with_simulated_tree_map: example script showing how to apply the registration algorithm for a simulated tree map of a forest. 
The user can vary, e.g., the area and stem density of the simulated forest and the standard deviation of the noise added to the tree locations. <br/>
-example_script_with_real_tree_map: example script illustrating how the registration method can be used to align a tree map obtained with a handheld 
laser scanner (in a local coordinate system) with a tree map obtained with an airborne laser scanner. Under the Data-subfolder, we 
provide stem locations detected from handheld laser scanning data as well as stem and tree top locations detected from airborne 
laser scanning data. <br/>

## Requirements for running the code <br/>
-Basic version of Matlab is required. The code has been tested on Matlab versions R2019a and R2020b. No additional toolboxes are required. 

## Did you encounter bugs when running the code or do you have suggestions for impromevements?
Please contact Eric Hyyppä (eric.hyyppa@gmail.com) and Jesse Muhojoki (jesse.muhojoki@nls.fi). We are also happy to hear about possible success 
stories, in which the method/code was utilized. 
