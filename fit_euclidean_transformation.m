function [theta, R_mat, t_vect, n_of_matches, feat_desc_cell_array] = ...
    fit_euclidean_transformation(xy_mat1, xy_mat2, parameters)
%This function finds the 2D Euclidean transformation between two point 
%clouds using the locations of some distinct objects detected from the point clouds.
%The Euclidean transformation is defined as xy_2 = R_mat*xy_1 + t_vect,
%where xy_1 is a 2-by-1 vector containing the (x,y)-coordinates of an object 
%in the first coordinate frame, xy_2 is a 2-by-1 vector containing the 
%(x,y)-coordinates of the same object in the second coordinate frame, 
%R_mat is a 2-by-2 rotation matrix, and t_vect is a 2-by-1 translation vector. 
%For example, the function can be used to perform coarse registration in 
%order to align a terrestrial point cloud of a forest (in a local 
%coordinate system) with an airbone-collected point cloud (in a global 
%coordinate system). For this application, it is useful to use the locations
%of trees detected from the point clouds for the registration task.

%The function is based on the algorithm proposed in Hyypp� and Muhojoki et
%al (2021), Efficient coarse registration method using translation- and 
%rotation-invariant local descriptors towards fully automated forest 
%inventory, ISPRS Open Journal of Photogrammetry and Remote Sensing.
%Relevant properties of the algorithm are the following:
%1) One does NOT need to know beforehand which object in the first 
%dataset corresponds to which object in the second data set. 
%2) The algorithm can deal with moderate omission and commission errors in 
%the object detection.
%3) Furthermore, the registration should work even if the point clouds have
%only partial overlap. (Overlap refers to the geometric overlap after
%aligning the point clouds).
%4) The algorithm runs in a quadratic time w.r.t. the number of detected
%objects
%5) No initial guess for the rotation angle or the translation vector are
%required. 

%The algorithm performs the following steps:
%1) For each detected object location, we construct an 8-element rotation- and
%translation-invariant feature descriptor vector based on the
%relative locations of the neighboring objects.
%2) For each of the feature descriptors in the second data set, we find the 
%most similar feature descriptor in the first dataset. These object pairs
%are regarded as tentative matching pairs. The dissimilarity is measured
%using Euclidean distance in the feature space.
%3) The tentative matching pairs are ranked using the nearest neighbor distance
%ratio.
%4) For each of the k most promising matching pairs, we determine an estimate
%of the Euclidean transformation and compute the number of matching object pairs 
%that are located within a distance of r_thres from each other after transforming the 
%objects in the first data set with the estimated transformation. The
%transformation with the highest number of matching object pairs is regarded
%as the best transformation. 
%5) We refine the best transformation by an iterative optimization algorithm 
%using the previously found matching object pairs. 

%NOTE: If one of the datasets contains many more objects than the other, it
%should be chosen as the dataset 1 and the dataset with a smaller number of
%objects should be dataset 2. This is for the fact that the algorithm tries
%to find the matches in the dataset 1 for each of objects in the dataset 2,
%and if the dataset 2 would contain many more objects than dataset 1 it is
%more difficult to find reliable tentative matches. 

%NOTE: xy2 = R_mat*xy_1 + t <-> xy1 = R_mat' * (xy2 - t)

%Args:
%   xy_mat1 = (x,y)-coordinates of objects (e.g. trees) found from point 
%             cloud 1 as a N1-by-2 matrix. (N1=number of rows, 2= number of
%             columns)
%   xy_mat2 = (x,y)-coordinates of objects (e.g. trees) found from point 
%             cloud 2 as N2-by-2 matrix. If one of the datasets has
%             significantly less objects, it should be chosen as dataset 2
%             (i.e., N2 < N1).  
%   parameters.R_local = radius of the local neighborhood used in 
%             constructing the feature descriptor of each object. The default
%             value is 10 (meters).
%   parameters.k = number of promising matching pairs to use for finding the
%             best Euclidean transformation. The default value is 20.
%   parameters.r_thres = distance threshold for considering an object pair as a
%             match. The default value is 1.0 (meters).
%   parameters.max_iter = maximum number of iterations in the refinement of
%             the parameter values. Default value is 200. 
%Returns:
%   theta = rotation angle (by which xy_1 should be rotated to obtain
%   xy_2)
%   R_mat = 2D rotation matrix corresponding to theta
%   t_vect = translation vector as a 2-by-1 column vector.
%   n_of_matches = number of matching object pairs for the best transformation
%   feat_desc_cell_array = cell array with two elements containing the
%   feature descriptors for all objects as N-by-8 matrices

%Set default parameter values
R_local = 10; 
k = 20;
r_thres = 1.0;
max_iter = 200; 

% Get parameters from input
if nargin > 2
    if ~isempty(parameters) % user may have given parameters
        if isfield(parameters, 'R_local')
            R_local = parameters.R_local;
        end
        if isfield(parameters, 'k')
            k = parameters.k;
        end
        if isfield(parameters, 'r_thres')
            r_thres = parameters.r_thres;
        end
        if isfield(parameters, 'max_iter')
            max_iter = parameters.max_iter;
        end
    end
end

%Centering the coordinates before matching to improve numerical stability
xy_mat1_mean = mean(xy_mat1, 1); %1-by-2
xy_mat2_mean = mean(xy_mat2, 1); %1-by-2
xy_mat1 = xy_mat1 - xy_mat1_mean;
xy_mat2 = xy_mat2 - xy_mat2_mean; 

%Number of objects detected from each point cloud
N_objects_vect = [length(xy_mat1(:, 1)), length(xy_mat2(:, 1))];

if N_objects_vect(2) < k
    k = N_objects_vect(2);
    msg = ['Number of objects in dataset 2 (and thus the potential number of matches) ', ...
           'is smaller than given parameter k. k was set to ', num2str(N_objects_vect(2))];
    warning(msg)
end

%First, we construct the feature descriptor for each object in each of
%the point clouds. 
[feat_desc_mat1, char_dirs1] = compute_feature_descriptors(xy_mat1, R_local); 
[feat_desc_mat2, char_dirs2] = compute_feature_descriptors(xy_mat2, R_local);
feat_desc_cell_array{1} = feat_desc_mat1;
feat_desc_cell_array{2} = feat_desc_mat2;

%Preallocating matrix for storing pairwise distances between feature 
%descriptors of the two point clouds -> rows correspond to objects detected
%from the point cloud 2 and columns correspond to objects detected from 
%the point cloud 1. 
feat_desc_pdist = zeros(N_objects_vect(2), N_objects_vect(1));
for i = 1 : length(feat_desc_mat1(1, :))
    feat_desc_pdist = feat_desc_pdist + (feat_desc_mat2(:, i) - feat_desc_mat1(:, i)').^2;
end
feat_desc_pdist = sqrt(feat_desc_pdist);

%For each object in point cloud 2, we find the most similar feature
%descriptor vector in point cloud 1 based on Euclidean distance in the
%feature space. This gives tentative matching pairs
[min_desc_dists, nn_indices] = min(feat_desc_pdist, [], 2);

%For each object in point cloud 2, we find the 2nd nearest neighbor feature
%descriptor in point cloud 1. 
for j = 1 : N_objects_vect(2)
    feat_desc_pdist(j, nn_indices(j)) = Inf;
end
[min2_desc_dists, ~] = min(feat_desc_pdist, [], 2);
%Computing the nearest neighbor distance ratio for objects in the point
%cloud 2 (the smaller the more reliable the tentative match is)
NNDR_vect = min_desc_dists./min2_desc_dists;
%Sorting the nearest neighbor distance ratios so that we can rank the
%tentative matches from most reliable to least reliable
[~, sorted_indices] = sort(NNDR_vect);

%Then, we consider the k most promising tentative matches 
max_n_of_matches = 0; %maximum number of matching objects
%Initializing the parameters for the transformation of the best match. 
t_best = zeros(1, 2);
theta_best = 0; 
idx_matches1_best = []; %indices of matching objects in point cloud 1 
idx_matches2_best = []; %indices of matching objects in point cloud 2 

for i_iter = 1 : k
    %indices of objects corresponding to the current tentative match
    idx_object_2 = sorted_indices(i_iter); 
    idx_object_1 = nn_indices(idx_object_2);
    %Characteristic directions corresponding to these objects
    char_dir1 = char_dirs1(idx_object_1, :);
    char_dir2 = char_dirs2(idx_object_2, :);
    %Computing the rotation angle based on the characteristic directions
    theta_magn = acos(sum(char_dir1.*char_dir2));
    theta_sign = sign(sum(cross([char_dir1, 0], [char_dir2, 0]).*[0,0,1]));
    theta = theta_magn*theta_sign;
    %The corresponding rotation matrix
    R_mat = [cos(theta), -sin(theta); sin(theta), cos(theta)];
    %Computing the translation vector based on the object locations in the two point
    %clouds
    xy_1 = xy_mat1(idx_object_1, :); %row vect.
    xy_2 = xy_mat2(idx_object_2, :); %row vect.
    t_vect = (xy_2' - R_mat*(xy_1')); %column vector

    %For each object in point cloud 2, we find the closest object in the 
    %point cloud 1 after using the estimated transformation. We also compute 
    %the number of matches, i.e., object pairs whose distance is below the 
    %set criterion
    [from_idx2_to_closest_idx1, from_idx2_to_min_dist1, n_of_matches] ...
    = get_closest_pairs_after_transformation(xy_mat1, xy_mat2, R_mat, t_vect, r_thres); 
    if(n_of_matches > max_n_of_matches)
        max_n_of_matches = n_of_matches;    
        indices2 = 1 : N_objects_vect(2);
        idx_matches2_best = indices2(from_idx2_to_min_dist1 < r_thres);
        idx_matches1_best = from_idx2_to_closest_idx1(idx_matches2_best);
        t_best = t_vect;
        theta_best = theta;
    end  
end

%Having found the best match, we refine the transformation by optimizing the 
%parameters of the Euclidean transformation using the found matching pairs
xy_mat1_matching = xy_mat1(idx_matches1_best, :); 
xy_mat2_matching = xy_mat2(idx_matches2_best, :); 

%Function to minimize: beta(1) = theta, beta(2) = t_x, beta(3) = t_y
obj_fun = @(beta) sum(sum( (([cos(beta(1)), -sin(beta(1)); sin(beta(1)), cos(beta(1))]*xy_mat1_matching' + ...
            [beta(2); beta(3)])' - xy_mat2_matching).^2, 2));
beta0 = [theta_best, t_best(1), t_best(2)];
%Optimization options
options = optimset('MaxIter', max_iter);
%Conducting the optimization
beta = fminsearch(obj_fun, beta0, options);
%optimized parameters for the Euclidean transformation
theta = beta(1);
R_mat = [cos(theta), -sin(theta); sin(theta), cos(theta)];
t_vect = [beta(2); beta(3)]; %column vector

%Computing the number of matches based on the refined transformation
[~, ~, n_of_matches] = get_closest_pairs_after_transformation(xy_mat1, xy_mat2, R_mat, t_vect, r_thres); 

%Undoing the centering to get the correct translation vector
t_vect = t_vect - (R_mat*xy_mat1_mean') + xy_mat2_mean'; %column vector
end


function [feat_desc_mat, char_dirs] = compute_feature_descriptors(xy_mat, R_local)
%Helper function for constructing the feature descriptor vectors for the 
%objects whose locations are listed in xy_mat.
%
%Args: 
%   xy_mat = (x,y)-coordinates of objects (e.g. trees) found from a point 
%           cloud as a N-by-2 matrix
%   R_local = radius of the local neighborhood used in 
%           constructing the feature descriptor of each object.
%
%Returns: 
%   feat_desc_mat = N-by-8 matrix containing the feature descriptor vectors
%           for each object. Each row corresponds to one object. The first
%           4 columns correspond to the normalized distance to the closest
%           object in each of the 4 quadrants, whereas the last 4 columns
%           correspond to the normalized angle between the closest object
%           in each of the 4 quadrants and the quadrant border. 
%   char_dirs = N-by-2 matrix containing the characteristic directions
%           for each object. Each row corresponds to one object. The 
%           charactistic direction for a given object is defined by the
%           spatial vector between the object itself and its nearest neighboring
%           object. 

%Pre-allocating the feature descriptor matrix
feat_desc_mat = zeros(length(xy_mat(:, 1)), 8); 
x_vect = xy_mat(:, 1);  %column vector
y_vect = xy_mat(:, 2);
%Pair-wise distances between all objects as a matrix. 
p_dist_mat = sqrt((x_vect' - x_vect).^2 + (y_vect' - y_vect).^2);
%Setting the "self-distance" to infinity
inf_eye = diag(Inf * ones(1,length(p_dist_mat(:, 1))));
p_dist_mat = p_dist_mat + inf_eye;
%Finding the closest neighboring object for each object
[~, idx_closest] = min(p_dist_mat,[],2); %outputs column vectors
%Coordinates of the closest neighboring object for each of the objects
x_closest = x_vect(idx_closest);  %column vector
y_closest = y_vect(idx_closest);

%For each object, compute the normalized characteristic directions that
%are locally used as the 1st axis direction. 
char_dirs = [x_closest - x_vect, y_closest - y_vect]; %N-by-2
char_dirs = char_dirs ./(sqrt(sum(char_dirs.^2, 2)));
%Directions perpendicular to the characteristic directions. These are
%used as the local 2nd axis direction. 
perp_char_dirs = [-char_dirs(:, 2), char_dirs(:, 1)];

%For each object, we transform the coordinates of the other objects
%into the local coordinate frame of the given object. We denote the
%coordinate along the 1st axis direction by v and the coordinate along
%the 2nd axis direction by w. In the matrices below, the element at row
%i and column j means the coordinate of the jth object in the local
%coordinate frame of the ith object. 
v_mat = (x_vect' - x_vect).*char_dirs(:, 1) + (y_vect' - y_vect).*char_dirs(:, 2);
w_mat = (x_vect' - x_vect).*perp_char_dirs(:, 1) + (y_vect' - y_vect).*perp_char_dirs(:, 2);
%Use atan2 to compute the angles with respect to the characteristic
%directions. The angles are in the range [-pi, pi]
angle_mat =  atan2(w_mat, v_mat);
%For each object, we then need to find the closest object in each of the 
%four quadrants corresponding to angles (0, pi/2), (pi/2, pi), (-pi, -pi/2), 
%(-pi/2, 0).
eps = 10^(-6); %used to exclude the closest object from the 1st/4th quadrant
for i_quadrant = 1 : 4
    if i_quadrant == 1
        quadrant_i_mat = (angle_mat > 0 + eps) & (angle_mat <= pi/2); %logical array
    elseif i_quadrant == 2
        quadrant_i_mat = (angle_mat > pi/2) & (angle_mat <= pi); %logical array
    elseif i_quadrant == 3
        quadrant_i_mat = (angle_mat > -pi) & (angle_mat <= -pi/2); %logical array
    else
        quadrant_i_mat = (angle_mat > -pi/2) & (angle_mat < 0-eps); %logical array
    end
    % Construct a matrix marking objects not belonging to the current
    % quadrant with Inf-values
    not_quadrant_i_mat = zeros(size(quadrant_i_mat));
    not_quadrant_i_mat(quadrant_i_mat == 0) = Inf; 
    %For each object, find the closest object within the current quadrant
    [min_dist_i, idx_closest_i] = min(p_dist_mat + not_quadrant_i_mat, [], 2);
    %For each object, determine the angle of the closest object in the
    %current quadrant w.r.t. the characteristic direction.
    angle_closest_quad_i = diag(angle_mat(:, idx_closest_i)); 
    %Normalizing the distances and angles 
    min_dist_i_norm = min_dist_i/R_local;
    if i_quadrant == 1
        angle_quad_i_norm = (angle_closest_quad_i - 0)/(pi/2);
    elseif i_quadrant == 2
        angle_quad_i_norm = (angle_closest_quad_i - pi/2)/(pi/2);
    elseif i_quadrant == 3
        angle_quad_i_norm = (angle_closest_quad_i - (-pi))/(pi/2);
    else
        angle_quad_i_norm = (angle_closest_quad_i - (-pi/2))/(pi/2);
    end
    %If the min distance in a given quadrant is larger than R_local, we set 
    %the normalized distance and angle to -1.
    min_dist_i_norm(min_dist_i > R_local) = -1;
    angle_quad_i_norm(min_dist_i > R_local) = -1;
    %Storing the normalized min distance and the angle to the pre-allocated
    %feature descriptor matrix
    feat_desc_mat(:, i_quadrant) = min_dist_i_norm; 
    feat_desc_mat(:, i_quadrant+4) = angle_quad_i_norm; 
end

end


function [from_idx2_to_closest_idx1, from_idx2_to_min_dist1, n_of_matches] ...
    = get_closest_pairs_after_transformation(xy_mat1, xy_mat2, R_mat, t_vect, r_thres)
%Helper function for obtaining a mapping from each object in dataset 2 to the
%index of the closest object in dataset 1 after transforming the object 
%locations in dataset 1 as R_mat*xy_mat1' + t_vect. 
%The function also evaluates the number of matching pairs by finding the 
%the number of closest object pairs, for which the pair-wise distance is below
%r_thres.
%
%Args:
%   xy_mat1 = (x,y)-coordinates of objects (e.g. trees) found from point 
%             cloud 1 as a N1-by-2 matrix. (N1=number of rows, 2= number of
%             columns)
%   xy_mat2 = (x,y)-coordinates of objects (e.g. trees) found from point 
%             cloud 2 as N2-by-2 matrix
%   R_mat = 2-by-2 rotation matrix of the transformation
%   t_vect = 2-by-1 column vector corresponding to the translation vector
%           of the transformation
%   r_thres = distance threshold for deciding if the closest object in the
%           other point cloud is a match
%Returns:
%   from_idx2_to_closest_idx1 = N2-by-1 vector containing the indices of the closest 
%           object in dataset 1 for each object in dataset 2. 
%   from_idx2_to_min_dist1 = N2-by-1 vector containing the distances to the closest 
%           object in dataset 1 for each object in dataset 2. 
%   n_of_matches = number of matching object pairs based on r_thres. 

xy_mat1_transformed = (R_mat*xy_mat1' + t_vect)'; %N-by-2
% Pair-wise locations between each object in dataset 1 and 2. Each row is
% one object in dataset 2 and each column in one object in dataset 1. 
transformed_loc_p_dist = sqrt((xy_mat2(:, 1) - xy_mat1_transformed(:,1)').^2 + ...
                    (xy_mat2(:, 2) - xy_mat1_transformed(:,2)').^2);
%Finding the closest object in transformed point cloud 1 for each object
%in point cloud 2
[from_idx2_to_min_dist1, from_idx2_to_closest_idx1] = min(transformed_loc_p_dist, [], 2);
%Computing the number of matches, i.e., object pairs whose distance is 
%below the set criterion
n_of_matches = sum(from_idx2_to_min_dist1 < r_thres);
end
