function [from_idx1_to_idx2] = find_matching_pairs(xy_mat1, xy_mat2, r_thres)
%This function finds a mapping from object locations in dataset 1 to the
%matching object locations in dataset 2. An object in the dataset 1 has a
%matching pair in data set 2 if the distance to the closest object 
%in the 2nd dataset is at most r_thres. 
%
%NOTE: We assume that the coordinates in xy_mat1 and xy_mat2 are in the
%same coordinate system

%Inputs:
%   xy_found = object locations in the first data set in the format [x1, y1; x2,
%   y2; ..] (N1-by-2)
%   xy_ref = object locations in the 2nd data set in the format [x1, y1; x2,
%   y2; ..] (N2-by-2)
%   r_thres = distance threshold for considering an object pair as a
%             match.
%Outputs
%   from_idx1_to_idx2 = mapping from the object indices in dataset 1 to the
%   object indices in dataset 2.
%   E.g. from_idx1_to_idx2(idx1) = idx2 means that the object at index idx1
%   in the first dataset corresponds to the object at index idx2 in the second
%   dataset. If no corresponding object exists, then from_idx1_to_idx2(idx1) = 0.

nof_1 = length(xy_mat1(:, 1)); 
nof_2 = length(xy_mat2(:, 1)); 

%Initializing the vector for storing the mapping from indices in dataset 1
%to indices in dataset 2. 
from_idx1_to_idx2 = zeros(1, nof_1);

%Now, we find the closest object in the dataset 2 for each object in
%dataset 1
for idx_1 = 1 : nof_1
    dist_closest = Inf;
    idx_2_closest = 0;
    for idx_2 = 1 : nof_2
        dist =  sqrt(sum((xy_mat1(idx_1, :) - xy_mat2(idx_2, :)).^2));
        if dist < dist_closest
            dist_closest = dist;
            idx_2_closest = idx_2;
        end
    end
    %Let's only match the objects if the distace between them is smaller than
    %the threshold
    if dist_closest <= r_thres
        from_idx1_to_idx2(idx_1) = idx_2_closest;
    else
        from_idx1_to_idx2(idx_1) = 0; %no object in dataset 2 is close enough
    end
end

end

