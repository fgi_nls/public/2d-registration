close all
clear all

%In this example script, we show how to find the Euclidean transformation
%between a point cloud collected with a handheld laser scanner (local 
%coordinate system) and a point cloud collected with an airborne laser
%scanner (global coordinate system). To this end, we use tree locations
%obtained from the point clouds and the coarse registration algorithm
%proposed in Hyypp� and Muhojoki et
%al (2021), Efficient coarse registration method using translation- and 
%rotation-invariant local descriptors towards fully automated forest 
%inventory, ISPRS Open Journal of Photogrammetry and Remote Sensing.


%Read the tree locations stored under the Data-folder
%1) Stem locations from a handheld (HH) laser scanning point cloud
file_path_HH_stems = ['.', filesep, 'Data', filesep, 'xy_coords_of_HH_stems.txt'];
xy_HH_stems_mat = table2array(readtable(file_path_HH_stems)); 
%2) Tree locations from an airborne laser scanning (ALS) point cloud.
% There are two alternatives: a) use stem locations detected from the ALS
% point cloud or b) use tree top locations detected from the ALS point
% cloud.
use_ALS_stem_locations = 1; %1: use ALS stem locations, 0: use ALS tree top location
if(use_ALS_stem_locations)
    %a) Stem location from an ALS point cloud
    file_path_ALS_stems = ['.', filesep, 'Data', filesep, 'xy_coords_of_ALS_stems.txt'];
    xy_ALS_mat = table2array(readtable(file_path_ALS_stems));
else
    %b) Tree top locations from an ALS point cloud 
    file_path_ALS_tops = ['.', filesep, 'Data', filesep, 'xy_coords_of_ALS_tops.txt'];
    xy_ALS_mat = table2array(readtable(file_path_ALS_tops)); 
end

%For plotting, we subtract the mean coordinates
xy_HH_mean = mean(xy_HH_stems_mat, 1); 
xy_ALS_mean = mean(xy_ALS_mat, 1); 

%Plot the tree locations before the registration 
figure; 
hold on
scatter(xy_ALS_mat(:, 1) - xy_ALS_mean(1), xy_ALS_mat(:, 2) - xy_ALS_mean(2), 7, 'or', 'filled')
scatter(xy_HH_stems_mat(:, 1) - xy_HH_mean(1), xy_HH_stems_mat(:, 2) - xy_HH_mean(2), 4, 'ok', 'filled')
set(gca, 'ticklength', 2*get(gca, 'ticklength'), 'Linewidth', 1.3, 'Fontsize', 16)
set(gca,'TickLabelInterpreter', 'latex')
xlabel('x (m)', 'Interpreter', 'latex', 'Fontsize', 18)
ylabel('y (m)', 'Interpreter', 'latex', 'Fontsize', 18)
leg = legend( 'ALS stems', 'HH stems', 'location', 'northwest');
set(leg, 'Interpreter', 'latex', 'Fontsize', 13)
title('Before registration','Interpreter', 'latex', 'Fontsize', 17);
box on
axis equal
pbaspect([1, 1, 1])

% Use the coarse registration algorithm to find the transformation
% from the local coordinate system of the HH scanner to the global coordinate
% system of the ALS point cloud. Since there are so many more trees in the 
% ALS data, the registration can be performed more reliably by using the ALS 
% data as dataset 1 and the HH data as dataset 2. 
parameters.R_local = 10;
parameters.k = 20; 
parameters.r_thres = 1.0; 
tic
[theta_est, R_mat_est, t_vect_est, nof_matches, feat_desc_cell_array] = ...
    fit_euclidean_transformation(xy_ALS_mat, xy_HH_stems_mat, parameters); 
toc
%Transforming the HH-derived tree locations to the global coordinate system
% xy_HH = R*xy_ALS + t -> xy_ALS = R' * (xy_HH - t)
xy_HH_stems_mat_transf = (R_mat_est' * (xy_HH_stems_mat' - t_vect_est))'; 

%Determining which HH stem corresponds to which ALS stem after
%transformation
[from_HH_idx_to_ALS_idx] = find_matching_pairs(xy_HH_stems_mat_transf, xy_ALS_mat, parameters.r_thres);
xy_mat_HH_transf_with_match = xy_HH_stems_mat_transf(from_HH_idx_to_ALS_idx > 0, :); 
xy_mat_HH_transf_without_match = xy_HH_stems_mat_transf(from_HH_idx_to_ALS_idx == 0, :);
indices_ALS_with_match = unique(from_HH_idx_to_ALS_idx); 
indices_ALS_without_match = setdiff([1 : length(xy_ALS_mat(:, 1))], indices_ALS_with_match); 
xy_mat_ALS_with_match = xy_ALS_mat(indices_ALS_with_match(2:end), :);  %Remove 0-element
xy_mat_ALS_without_match = xy_ALS_mat(indices_ALS_without_match, :); 
 
%Plot the tree locations after the registration. We subtract the mean ALS
%coordinate from both coordinate sets
figure; 
hold on
scatter(xy_mat_ALS_without_match(:, 1) - xy_ALS_mean(1), xy_mat_ALS_without_match(:, 2) - xy_ALS_mean(2), 30, 'or', 'filled')
scatter(xy_mat_ALS_with_match(:, 1) - xy_ALS_mean(1), xy_mat_ALS_with_match(:, 2) - xy_ALS_mean(2), 30, 'og', 'filled')
scatter(xy_mat_HH_transf_without_match(:, 1) - xy_ALS_mean(1), xy_mat_HH_transf_without_match(:, 2)- xy_ALS_mean(2), 15, 'ok', 'filled')
scatter(xy_mat_HH_transf_with_match(:, 1) - xy_ALS_mean(1), xy_mat_HH_transf_with_match(:, 2)- xy_ALS_mean(2), 15, 'ob', 'filled')
set(gca, 'ticklength', 2*get(gca, 'ticklength'), 'Linewidth', 1.3, 'Fontsize', 16)
set(gca,'TickLabelInterpreter', 'latex')
xlabel('x (m)', 'Interpreter', 'latex', 'Fontsize', 18)
ylabel('y (m)', 'Interpreter', 'latex', 'Fontsize', 18)
leg = legend( 'ALS: no match', 'ALS: with match', 'HH: no match', 'HH: with match', 'Location', 'northwest');
set(leg, 'Interpreter', 'latex', 'Fontsize', 13)
title('After registration','Interpreter', 'latex', 'Fontsize', 17);
box on
axis equal
xmin = min(xy_HH_stems_mat_transf(:,1) - xy_ALS_mean(1)) - 15;
xmax = max(xy_HH_stems_mat_transf(:,1) - xy_ALS_mean(1)) + 15;
ymin = min(xy_HH_stems_mat_transf(:,2) - xy_ALS_mean(2)) - 10;
ymax = max(xy_HH_stems_mat_transf(:,2) - xy_ALS_mean(2)) + 35;
xlim([xmin, xmax])
ylim([ymin, ymax])
pbaspect([1, 1, 1])